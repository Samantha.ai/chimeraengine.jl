#!/usr/bin/env julia

using Pkg, Pkg.TOML
using Sockets
using ArgParse

function parse()
  settings = ArgParseSettings(
    description = "cectl - ChimeraEngine Controller",
    epilog = "Please report bugs to https://gitlab.com/Samantha.ai/ChimeraEngine.jl",
    version = "Version 0.0.1",
    add_version = true,
    exc_handler = ArgParse.debug_handler
  )

  @add_arg_table settings begin
    "--start"
      action = :store_true
      help = "Start ChimeraEngine if not running"
    "--stop"
      action = :store_true
      help = "Stop ChimeraEngine if running"
    "--config", "-C"
      help = "Path to config file"
    "--debug", "-D"
      action = :store_true
      help = "Debug mode; outputs lots of information"
    "--add-hosts"
      nargs = '*'
      help = "Add one or more hosts"
    "--rm-hosts"
      nargs = '*'
      help = "Remove one or more hosts"
    "--add-projects"
      nargs = '*'
      help = "Add one or more projects"
    "--rm-projects"
      nargs = '*'
      help = "Remove one or more projects"
  end

  return parse_args(settings)
end

function load_config(opts, config_path)
  config_file = open(config_path, "r+")
  config = TOML.parse(config_file)
  return config, config_file
end
function update_config(opts, config)
  for connstr in opts["add-hosts"]
    idx = findfirst(str->str==connstr, config["hosts"])
    # TODO: Should we throw an error on adding a duplicate host?
    if idx === nothing
      push!(config["hosts"], connstr)
    end
  end
  for connstr in opts["rm-hosts"]
    idx = findfirst(str->str==connstr, config["hosts"])
    if idx === nothing
      error("Host not found: $connstr")
    end
    deleteat!(config["hosts"], idx)
  end
  return config
end
function write_config!(config_file, opts, config)
  seek(config_file, 0)
  truncate(config_file, 0)
  TOML.print(config_file, config)
end
function controller_start(opts, config_path)
  run(`julia -e "using ChimeraEngine; ChimeraEngine.redirect_logs(); println(getpid()); socket_listen(launch(\"$config_path\"))"`; wait=false)
end
function controller_stop(opts, sock)
  println(sock, "stop")
end
function controller_reload(opts, sock)
  println(sock, "reload")
end
function main()
  opts = parse()
  debug = opts["debug"]
  if debug
    println("Options:")
    for (k,v) in opts
      println("  $k => $v")
    end
  end

  if opts["config"] !== nothing
    config_path = opts["config"]
  else
    error("Config autofinding not yet implemented")
  end
  @assert ispath(config_path) "Config does not exist at $config_path"

  sock_path = "/tmp/chimeraengine/controller.sock"
  if ispath(sock_path)
    sock = connect(sock_path)
    running = true
  else
    sock = nothing
    running = false
  end

  if opts["start"]
    if running
      println("Error: Controller already running")
      exit(1)
    else
      controller_start(opts, config_path)
      running = true
      while !ispath(sock_path)
        sleep(1)
      end
      sock = connect(sock_path)
    end
  elseif opts["stop"]
    if !running
      println("Error: Controller not running")
      exit(1)
    else
      controller_stop(opts, sock)
      running = false
      sock = nothing
    end
  end
  old_config, config_file = load_config(opts, config_path)
  if debug
    println("Old config:")
    for (k,v) in old_config
      println("  $k => $v")
    end
  end
  new_config = update_config(opts, old_config)
  if debug
    println("New config:")
    for (k,v) in new_config
      println("  $k => $v")
    end
  end
  write_config!(config_file, opts, new_config)
  close(config_file)
  if running
    controller_reload(opts, sock)
  end

  if sock !== nothing && isopen(sock)
    close(sock)
  end
  exit(0)
end
main()
