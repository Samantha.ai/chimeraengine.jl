"Storage for a set of candidates."
struct CandidateStorage{C}
  history::Vector{C}
end

"A transition to a new model, with initial params."
struct ModelTransition
  model
  params
end

"A transition to a new optimizer."
struct OptimizerTransition
  opt
end

"A transition to a new model transformer."
struct ModelTransformerTransition
  model_trans
end

"""
    PolicyStorage

Storage for all events occuring under the purview of a policy.
"""
struct PolicyStorage
  # TODO: Add metadata, like timestamps, host, etc?
  history::Vector
end

"Truncation strategy which simply deletes truncated data."
struct DeleteTruncation end

"The list of truncation strategies to be attempted in order."
global const CE_TRUNCATION_LIST = Ref(Any[DeleteTruncation()])
