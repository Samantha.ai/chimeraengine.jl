module ChimeraEngine

using Distributed
using UUIDs
using Dates
using Pkg, Pkg.TOML
using Sockets
using Requires

export launch

include("types.jl")
include("traits.jl")
include("api.jl")
include("macros.jl")
include("registration.jl")
include("storage.jl")
include("loader.jl")
include("network.jl")
include("hooks.jl")
include("frontier.jl")
include("recorder.jl")
include("defaults.jl")
include("worker.jl")
include("controller.jl")

function launch(config_path=nothing)
  if config_path === nothing
    # Attempt to get config path from environment
    if haskey(ENV, "CHIMERAENGINE_CONFIG_PATH")
      config_path = ENV["CHIMERAENGINE_CONFIG_PATH"]
    else
      error("No config specified; try setting the environment variable CHIMERAENGINE_CONFIG_PATH")
    end
  end
  @assert ispath(config_path) "Config does not exist at $config_path"

  ctrl = Controller(config_path)
  @show ctrl.config
  load_config!(ctrl)
  @show ctrl.config

  return ctrl
end

function redirect_logs()
  mkpath("/tmp/chimeraengine")
  logfile = open("/tmp/chimeraengine/controller.log", "a")
  redirect_stdout(logfile)
  redirect_stderr(logfile)
end

function __init__()
  include(joinpath(@__DIR__, "requires.jl"))
end

end # module
