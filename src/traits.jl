### Generic Traits ###

"""Indicates whether `x` is a valid optimizer"""
isoptimizer(x) = false

"""Indicates whether `x` is a valid model"""
ismodel(x) = false

### Optimizer Traits ###

"""Indicates whether optimizer `opt` can optimize a model of type `model_type`"""
canoptimizemodel(opt, model_type) = false

"""Indicates whether optimizer `opt` can generate a seed model of type `model_type`"""
cangenerateseedmodel(opt, model_type) = false

### Model Traits ###

"""Indicates whether model `model` has optimizable parameters"""
hasoptimizableparams(model) = false
