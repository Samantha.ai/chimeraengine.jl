export run_worker, simulate_worker

function load_project!(config, state, project_path)
  # Load project if not loaded already
  if !haskey(state, "project_module")
    # Activate project directory
    Pkg.API.activate(project_path)

    # FIXME: This is somewhat of a hack
    project_path = rstrip(project_path, '/')
    project_name = basename(project_path)
    @info "Loading $project_name"
    proj = Base.require(Main, Symbol(project_name))
    state["project_module"] = proj
  else
    proj = state["project_module"]
    # TODO: Something like Revise.revise(state["proj"])
  end

  # Update config
  new_config = Base.invokelatest(proj.get_config)
  merge!(config, new_config)

  # Generate history and data record
  if !haskey(state, "record")
    state["record"] = Base.invokelatest(initialize_record, config)
  else
    error("FIXME: Check for and change record if necessary")
  end

  # Load problem definition
  state["prob_def"] = Base.invokelatest(proj.get_problem)
end
function get_problem_parameters(prob_def, supplied_params)
  prob_params = Dict{Symbol,Any}()
  for param_def in prob_def.param_defs
    prob_params[param_def.name] = get(supplied_params, param_def.name, param_def.default)
  end
  return prob_params
end

function run_worker()
  comms = WorkerComms()
  set_comms!(comms)

  schedule(@task begin
    try
      # Send worker info (hardware, software versions, etc.) to server
      worker_announce_configuration(comms)

      # Set initial config and state
      config = Dict{String,Any}()
      state = Dict{String,Any}()

      while true
        # Read message from controller
        pkt = take!(comms)
        cmd, data = pkt

        if cmd == :worker_project_path
          # Load new project
          load_project!(config, state, data)
        elseif cmd == :exit
          exit(0)
        else
          @error("Unknown command: $cmd")
        end

        # Run for as long as possible
        Base.invokelatest(loop_worker, comms, state, config)
        do_hook!(:outer_complete)
      end
    catch err
      showerror(stdout, err); println()
      Base.show_backtrace(stdout, catch_backtrace()); println()
      # TODO: Waiting until exception stacks are backported
      #for (e,bt) in catch_stack()
      #  showerror(stdout, e, bt); println()
      #end
      return
    end
  end)

  reverse(comms)
end

function loop_worker(comms, state, config)
  try
    model = nothing
    prob_def = state["prob_def"]
    policy = generate_toplevel_policy(config)
    record = state["record"]

    # Initialize model transformer and optimizer
    @info "Worker Registers:" model_transformers=getregistered(:model_transformer) optimizers=getregistered(:optimizer)
    policy_init!(state, policy, config)
    do_hook!(:policy_init_complete)
    @info state["model_trans_def"]
    @info state["opt_def"]
    model_trans_def = state["model_trans_def"]
    opt_def = state["opt_def"]

    while true
      # Generate a new model and initial parameters
      # FIXME: Pass in frontier or something to pull models from
      (model, params), transform = generate_model(model_trans_def, prob_def.iface_defs, model)
      do_hook!(:generate_model_complete)
      record!(record, transform)
      @info model

      # Construct ModelDefinition
      model_def = ModelDefinition(model, prob_def.iface_defs)

      # Optimize model parameters over objective to acquire candidates
      candidates, optimization = optimize_model(opt_def, prob_def.obj_defs, prob_def.run_func, model_def, params)
      do_hook!(:optimize_model_complete)
      record!(record, optimization)

      # FIXME: Insert candidates into frontier

      # Perform network updates
      # FIXME: state = worker_update_state(comms, state)

      # Apply updates (objective file, optimization timeout, worker list, etc.)
      # FIXME: apply_state!(state)

      # Update model transformer and optimizer based on policy
      policy_update!(state, policy)
      do_hook!(:policy_update_complete)

      # Bail out on message from controller
      if isready(comms)
        return
      end

      do_hook!(:inner_complete)
      #put!(comms, (:one_and_done,))
      #return
    end
  catch err
    println("error!")
    showerror(stdout, err); println()
    Base.show_backtrace(stdout, catch_backtrace()); println()
    # TODO: Waiting until exception stacks are backported
    #for (e,bt) in catch_stack()
    #  showerror(stdout, e, bt); println()
    #end
    return
  end
end

generate_toplevel_policy(config::Dict) =
  config["toplevel_policy"]
generate_optimizer(config::Dict) =
  config["initial_optimizer"]
generate_model_transformer(config::Dict) =
  config["initial_model_transformer"]

"Fallback to generate model from scratch based on `obj_def`"
generate_model(obj_def, model_trans, model::Nothing) =
  generate_model(obj_def, model_trans)

"Simple function to run a single worker pass"
function simulate_worker(project_path; run_hooks=false, do_record=false)
  config, state = Base.invokelatest(_setup_worker, project_path;
    run_hooks=run_hooks,
    do_record=do_record,
  )
  Base.invokelatest(_simulate_worker, config, state;
    run_hooks=run_hooks,
    do_record=do_record,
  )
  return config, state
end
function _setup_worker(project_path; run_hooks=false, do_record=false)
  config = Dict{String,Any}()
  state = Dict{String,Any}()

  # Load project
  load_project!(config, state, project_path)

  # Setup model
  state["model"] = nothing

  # Setup policy
  policy = generate_toplevel_policy(config)
  state["policy"] = policy
  policy_init!(state, policy, config)
  run_hooks && do_hook!(:init_policy_complete)

  # Setup parameters
  state["prob_params"] = get(config, "parameters", NamedTuple())

  # Setup debug hook
  state["debug_hook"] = get(config, "debug_hook", nothing)

  # Setup frontier
  state["frontier"] = CandidateFrontier()

  # Setup frontier limit and eps
  state["frontier_limit"] = get(config, "frontier_limit", 100)
  state["frontier_eps"] = get(config, "frontier_eps", 0.0)

  return config, state
end
function _generate_initial_model(config, state)
  model = state["model"]
  prob_def = state["prob_def"]
  model_trans_def = state["model_trans_def"]

  model, (params, bounds), transform = generate_model(
    model_trans_def,
    prob_def.iface_defs,
    model
  )
  state["model"] = model
end
function _simulate_worker(config, state; run_hooks=false, do_record=false)
  model = state["model"]
  prob_def = state["prob_def"]
  record = state["record"]
  model_trans_def = state["model_trans_def"]
  opt_def = state["opt_def"]
  policy = state["policy"]
  prob_params = get_problem_parameters(prob_def, state["prob_params"])
  debug_hook = state["debug_hook"]
  frontier = state["frontier"]
  frontier_limit = state["frontier_limit"]
  frontier_eps = state["frontier_eps"]

  model, (params, bounds), transform = generate_model(
    model_trans_def,
    prob_def.iface_defs,
    model
  )
  state["model"] = model
  run_hooks && do_hook!(:generate_model_complete)
  do_record && record!(record, transform)

  model_def = ModelDefinition(model, prob_def.iface_defs)

  candidates, optimization = optimize_model(
    opt_def,
    prob_def.obj_defs,
    prob_def.run_func,
    model_def,
    (params, bounds),
    prob_params,
    debug_hook,
  )
  run_hooks && do_hook!(:optimize_model_complete)
  do_record && record!(record, optimization)

  # Merge frontiers and purge excess candidates
  added, deleted = pareto_merge!(frontier, candidates, frontier_eps)
  purge_excess_candidates!(frontier, frontier_limit)
  println("Added: $(length(added)), Deleted: $(length(deleted))")
  run_hooks && do_hook!(:update_frontier_complete)
  # FIXME: do_record && record_frontier!(record, frontier, added, deleted)

  policy_update!(state, policy)
  if run_hooks
    do_hook!(:update_policy_complete)
    do_hook!(:inner_complete)
    do_hook!(:outer_complete)
  end
end
function _simulate_problem(config, state)
  model = state["model"]
  prob_def = state["prob_def"]
  model_def = ModelDefinition(model, prob_def.iface_defs)
  params, bounds = get_params(model_def)
  prob_params = get_problem_parameters(prob_def, state["prob_params"])
  debug_hook = state["debug_hook"]

  model_def = apply_params(model_def, params)
  meta = (
    objectives = Dict{Symbol,Any}(),
    parameters = prob_params,
    debug_hook = debug_hook,
  )
  results = prob_def.run_func(model_def, meta)
  return CandidateSet(
    model,
    [Candidate(params, results)],
    prob_def.obj_defs
  ), nothing
end
