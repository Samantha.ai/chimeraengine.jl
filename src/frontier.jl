### Methods ###

Base.push!(set::CandidateSet, cands::Vararg{Candidate}) =
  push!(set.candidates, cands...)
Base.push!(frontier::CandidateFrontier, sets::Vararg{CandidateSet}) =
  push!(frontier.sets, sets...)

function Base.in((set, cand_idx), frontier::CandidateFrontier)
  for fset in frontier.sets
    fset.model != set.model && continue
    for fidx in 1:length(fset)
      fparams = fset.candidates[fidx].params
      params = set.candidates[cand_idx].params
      all(fparams .== params) && return true
    end
  end
  return false
end

Base.length(set::CandidateSet) =
  length(set.candidates)
function Base.length(frontier::CandidateFrontier)
  count = 0
  for set in frontier.sets
    count += length(set)
  end
  return count
end

"Merges a set of candidates into a frontier, subject to Pareto optimality."
function pareto_merge!(frontier::CandidateFrontier, set::CandidateSet, eps::Float64)
  added = CandidateSet(set.model, similar(set.candidates, 0), set.obj_defs)
  deleted = CandidateFrontier()
  to_delete = CandidateFrontier()
  for (cidx,cand) in enumerate(set.candidates)
    does_dominate = false
    is_dominated = false
    for (fsidx,fset) in enumerate(frontier.sets)
      _to_delete = Int[]
      for (fcidx,fcand) in enumerate(set.candidates)
        if dominates(fcand, cand)
          is_dominated = true
          break
        elseif dominates(cand, fcand)
          does_dominate = true
          push!(added.candidates, deepcopy(cand))
          push!(_to_delete, fcidx)
        end
      end
      is_dominated && break
      if !isempty(_to_delete)
        dset = CandidateSet(fset.model, similar(fset.candidates, 0), fset.obj_defs)
        for fcidx in reverse(_to_delete)
          push!(dset, fset.candidates[fcidx])
          deleteat!(fset.candidates, fcidx)
        end
        push!(to_delete, fsidx=>dset)
      end
    end
    # Are we non-dominated, but not yet added?
    if !is_dominated && !does_dominate
      push!(added.candidates, deepcopy(cand))
    end
  end
  # Purge empty frontier sets
  for fsidx in map(first, reverse(to_delete.sets))
    if isempty(frontier.sets[fsidx].candidates)
      deleteat!(frontier.sets, fsidx)
    end
  end
  return added, deleted
end

# FIXME: Remove this
"Merges a set of candidates into a frontier, subject to Pareto optimality."
function old_pareto_merge!(frontier::CandidateFrontier, set::CandidateSet, eps::Float64)
  new_candidates = CandidateSet(candidates.model, copy(candidates.params))
  added = FrontierCandidates(candidates.model, similar(candidates.params, 0))
  deleted = FrontierCandidates[]
  for new_params_idx in 1:nrow(new_candidates.params)
    does_dominate = false
    is_dominated = false

    new_params = new_candidates.params[new_params_idx,:]
    for (old_cands_idx, old_candidates) in enumerate(frontier)
      old_params_idxs_to_delete = Int[]
      for old_params_idx in 1:nrow(old_candidates.params)
        old_params = old_candidates.params[old_params_idx,:]
        if dominates(old_params, new_params)
          is_dominated = true
          break
        elseif dominates(new_params, old_params)
          does_dominate = true
          append!(added.params, new_params)
          push!(old_params_idxs_to_delete, old_params_idx)
        end
      end
      is_dominated && break
      if length(old_params_idxs_to_delete) > 0
        push!(deleted, FrontierCandidates(old_candidates.model, old_candidates.params[old_params_idxs_to_delete,:]))
        deleterows!(old_candidates.params, old_params_idxs_to_delete)
      end
    end
    if !is_dominated && !does_dominate
      append!(added.params, new_params)
    end

    # Remove empty old_candidates
    for old_cands_idx in length(frontier):-1:1
      old_candidates = frontier[old_cands_idx]
      if nrow(old_candidates.params) == 0
        deleteat!(frontier, old_cands_idx)
      end
    end
  end
  # TODO: Check for identical models and merge
  if nrow(added.params) > 0
    push!(frontier, FrontierCandidates(new_candidates.model, copy(added.params)))
  end
  return (added, deleted)
end
# FIXME: eps argument
function dominates(cdom::Candidate, csub::Candidate)
  # From Tim Wheeler's "Algorithms for Optimization"
  # https://github.com/tawheeler/alg4opt_test.jl
  all(csub.params .- cdom.results .≥ 0) && any(csub.results .- cdom.results .> 0)
end

"Flattens a frontier, assuming that all models are identical."
function _flatten_frontier(frontier::CandidateFrontier)
  seed_set = first(frontier.sets)
  new_set = CandidateSet(seed_set.model, similar(seed_set.candidates, 0), seed_set.obj_defs)
  for set in frontier.sets
    for idx in 1:length(set)
      cand = set.candidates[idx]
      push!(new_set.candidates, deepcopy(cand))
    end
  end
  return CandidateFrontier([new_set])
end

function purge_excess_candidates!(frontier, frontier_limit)
  while true
    lf = length(frontier)
    lf <= frontier_limit && break
    set_lengths = map(length, frontier.sets)
    idx = rand(1:lf)
    ctr = 1
    for (set_idx,set) in enumerate(frontier.sets)
      for cand_idx in 
        if ctr === idx
          # Purge candidate
          deleteat!(set.candidates, cand_idx)
          ctr = 0
          break
        else
          ctr += 1
        end
      end
      if ctr === 0
        if length(set) === 0
          # Clean up empty sets
          deleteat!(frontier.sets, set_idx)
        end
        break
      end
    end
  end
end
