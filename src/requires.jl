@require BlackBoxOptim="a134a8b2-14d6-55f6-9291-3336d3ab0209" begin
  export CEOpt_BlackBoxOptim_BorgMOEA

  struct CEOpt_BlackBoxOptim_BorgMOEA end
  register!(:optimizer, CEOpt_BlackBoxOptim_BorgMOEA)
  function optimize_model(opt::CEOpt_BlackBoxOptim_BorgMOEA,
      obj_defs,
      run_func,
      model,
      (params, bounds),
      prob_params,
      debug_hook)
    No = length(obj_defs)
    Np = length(params)
    results = BlackBoxOptim.bboptimize(;
        Method=:borg_moea,
        FitnessScheme=BlackBoxOptim.ParetoFitnessScheme{No}(is_minimizing=true),
        SearchRange=bounds,
        NumDimensions=Np,
        ϵ=0.05,
        #=MaxTime=5.0,=#
        TraceInterval=5.0,
        TraceMode=:verbose) do params
      model = apply_params(model, copy(params))
      meta = (
        objectives = Dict{Symbol,Any}(),
        parameters = prob_params,
        debug_hook = debug_hook,
      )
      run_func(model, meta)
      objs = Float64[]
      for def in obj_defs
        push!(objs, meta.objectives[def.name])
      end
      return tuple(objs...)
    end
    cands = map(cand->Candidate(BlackBoxOptim.params(cand), BlackBoxOptim.fitness(cand)), BlackBoxOptim.pareto_frontier(results))
    return CandidateSet(model.model, cands, obj_defs), nothing
  end
end

@require CSV="336ed68f-0bac-5ca0-87d4-7b16caf5d00b" begin
  export CSVRecorder

  struct CSVRecorder
    corefile_path::String
  end
  register!(:history_recorder, CSVRecorder)
  function CSVRecorder(config)
    dirpath = config["dirpath"]
    corefile_path = joinpath(dirpath, "core.csv")
    if ispath(corefile_path)
      # TODO: Validate history fields
    else
      @info "CSV write headers to $corefile_path"
      touch(corefile_path)
      initial_data = generate_initial_history()
      CSV.write(corefile_path, [initial_data])
    end
    return CSVRecorder(corefile_path)
  end
  function record!(cr::CSVRecorder, data_recorder, data)
    datapath = record!(data_recorder, data)
    @info "CSV write data to $(cr.corefile_path)"
    CSV.write(cr.corefile_path, [(
      timestamp = now(),
      datapath = datapath,
     )]; append=true)
  end
  function Base.iterate(cr::CSVRecorder)
    f = CSV.File(cr.corefile_path)
    result = Base.iterate(f)
    result === nothing && return nothing
    row, state = result
    return row, (f, state)
  end
  function Base.iterate(cr::CSVRecorder, state)
    result = Base.iterate(state[1], state[2])
    result === nothing && return nothing
    row, new_state = result
    return row, (state[1], new_state)
  end
  Base.length(cr::CSVRecorder) =
    length(CSV.File(cr.corefile_path))
  function Base.eltype(cr::CSVRecorder)
    f = CSV.File(cr.corefile_path)
    return eltype(f)
  end
end

@require BSON="fbb218c0-5317-5bc6-957e-2ee96dd4b1f0" begin
  export BSONRecorder

  struct BSONRecorder
    dirpath::String
  end
  register!(:data_recorder, BSONRecorder)
  function BSONRecorder(config)
    dirpath = config["dirpath"]
    BSONRecorder(dirpath)
  end
  function record!(br::BSONRecorder, data)
    filepath = joinpath(br.dirpath, (string(now()) * ".bson"))
    BSON.bson(filepath, Dict(:data=>data))
    return filepath
  end
  function Base.getindex(br::BSONRecorder, history_entry)
    datapath = joinpath(br.dirpath, history_entry.datapath)
    @assert ispath(datapath) "Not a file: $datapath"
    return BSON.load(datapath)
  end
  Base.eltype(br::BSONRecorder) = Dict{Symbol,Any}
end

#=
@require Flux="587475ba-b771-5e3f-ad9e-33799f191a9c" begin
end

@require SamanthaOptimizer="bbe43584-863f-11e8-3229-4145e5d2162a" begin
end
=#
