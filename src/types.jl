### Exports ###

export InterfaceDefinition, ObjectiveDefinition, ParameterDefinition, ProblemDefinition
export ArrayInterface
export Candidate, CandidateSet, CandidateFrontier
export isinput, isoutput

### Types ###

"""
    InterfaceDefinition{T,N}

Defines an interface to a model.

Arguments:
  `dir::Symbol`
    A symbol which is either `:in`, `:out`, or `:both`. Defines the
    directionality of the interface.
  `typ::Type{T}`
    FIXME
"""
struct InterfaceDefinition{T,M}
  name::Symbol
  dir::Symbol
  # FIXME: IIP vs OOP
  typ::Type{T}
  meta::M
end

isinput(iface::InterfaceDefinition) = iface.dir==:in
isoutput(iface::InterfaceDefinition) = iface.dir==:out

struct ArrayInterface{N}
  size::NTuple{N,Int}
end

# TODO: Docstring
struct InterfaceView{M}
  # FIXME: Direction
  model::M
  sym::Symbol
end

# TODO: Docstring
struct ModelDefinition{M}
  model::M
  #params::P
  iface_defs::Vector{InterfaceDefinition}
end

get_params(model_def::ModelDefinition) =
  get_params(model_def.model)
apply_params(model_def::ModelDefinition, params) =
  ModelDefinition(apply_params(model_def.model, params), model_def.iface_defs)
ce_run_model(model_def::ModelDefinition) =
  ce_run_model(model_def.model)

"""
    ObjectiveDefinition

Defines an objective.
"""
struct ObjectiveDefinition{T}
  name::Symbol
  typ::Type{T}
end

"""
    ParameterDefinition

Defines an parameter for controlling the problem.
"""
struct ParameterDefinition{T}
  name::Symbol
  typ::Type{T}
  default::T
end

"""
    ProblemDefinition{OD<:NamedTuple}

Fully specifies a problem, how to simulate a solution to the problem,
and how to rank a model's performance after the simulation completes.
"""
struct ProblemDefinition
  iface_defs::Vector{InterfaceDefinition}
  obj_defs::Vector{ObjectiveDefinition}
  param_defs::Vector{ParameterDefinition}
  run_func::Function
end

"A candidate, containing parameters and objective results."
struct Candidate{P,R}
  uuid::UUID
  params::P
  results::R
end
Candidate(params, results) =
  Candidate(uuid4(), params, results)

"A set of candidates all based on a single model."
struct CandidateSet{M,C<:Candidate}
  uuid::UUID
  model::M
  candidates::Vector{C}
  obj_defs::Vector{ObjectiveDefinition}
end
CandidateSet(model, candidates, obj_defs) =
  CandidateSet(uuid4(), model, candidates, obj_defs)

"A frontier of candidate sets."
struct CandidateFrontier
  sets::Vector{CandidateSet}
end
CandidateFrontier() =
  CandidateFrontier(CandidateSet[])

"""
    OptimizerDefinition{Opt}

Contains an optimizer, its algorithm, and any parameters.
"""
struct OptimizerDefinition{O,A,P}
  optimizer::O
  algorithm::A
  params::P
end

# TODO: Docs
struct ModelTransformerDefinition{MT}
  model_trans::MT
end

# TODO: Docs
struct PolicyDefinition{P}
  policy::P
end
