#= TODO: The Server
What the server does:

Launches and tracks worker processes
Sends roles (optimizer, analyzer, etc.) to workers
Sends configuration updates to workers
Exposes current state via API
Follows user-specified policy (how?)

User-specified policy:

Location of objective definition files
Usage policy of available worker hardware
API configuration details (listener port, etc.)

Why not do more?

The server is intended to be the cornerstone for the entire system. It must be robust, for if it fails for any reason, the entire simulation will likely terminate rapidly.
Therefore, it does the minimum amount of work necessary to keep things running, to prevent the possibility of unrecoverable errors.
Specifically, the server does not load simulation-related packages, and does not actively update its configuration via something like Revise.
In time, the above-listed functionalities should be moved out of the server to further increase the servers resiliance.
=#

function run_server(server)
  for w in server.workers
    # Launch optimizer task and fetch comms
    comm = fetch(@spawnat w begin
      run_optimizer()
    end)
    data_out, data_in = comm
    task = @task begin
      # FIXME: Send worker role(s) and configuration
      #=put!(data_out, (
        objective = server.objective_path,
        config = server.config)
      )=#
      while server.running
      end
    end
          if !isempty(server.frontier)
            # Pick a new candidate randomly
            candidate = rand(server.frontier)
            model = deepcopy(candidate.model)
          else
            model = deepcopy(server.seed.model)
          end
          # TODO: Supply Pareto frontier
          model = apply_operator(model, server.operators, server.seed.constraints, server.history)

          # Send seed model
          # TODO: Also send params
          put!(data_out, (:seed_model, (model, server.seed.interface)))

          # Evaluate fitness
          put!(data_out, (:run,))
          obj_keys, candidates_df = take!(data_in)

          # Uniquely label each candidate
          insert!(candidates_df, 1, map(x->Base.Random.uuid1(), 1:nrow(candidates_df)), :uuid)

          # Merge frontiers
          added, deleted = pareto_merge!(server.frontier, FrontierCandidates(model, candidates_df), obj_keys)

          # Update history
          update_history!(server.history, added, deleted)

          # Purge excess candidates
          purge_excess_candidates!(server.frontier, server.frontier_limit)

          # Evaluate callbacks with changes
          for callback in server.callbacks
            callback(server, added, deleted)
          end
        end
      catch err
        err isa InterruptException || rethrow(err)
      end
    end
    server.worker_comms[w] = comm
    server.worker_tasks[w] = task
    schedule(task)
  end
end
