export register!, isregistered, getregistered

global const REGISTRATIONS = Ref(Dict{Symbol,Vector{Any}}(
  :model_transformer => [],
  :optimizer => [],
  :policy => [],
  :model => [],
  :history_recorder => [],
  :data_recorder => [],
))

function register!(kind::Symbol, ::Type{T}) where T
  @assert haskey(REGISTRATIONS[], kind) "Registration of kind $kind not supported"
  push!(REGISTRATIONS[][kind], T)
end

function isregistered(kind::Symbol, ::Type{T}) where T
  @assert haskey(REGISTRATIONS[], kind) "Registration of kind $kind not supported"
  return T in REGISTRATIONS[][kind]
end

function getregistered(kind::Symbol)
  @assert haskey(REGISTRATIONS[], kind) "Registration of kind $kind not supported"
  return REGISTRATIONS[][kind]
end
