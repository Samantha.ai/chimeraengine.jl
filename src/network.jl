"""
    WorkerComms

A pair of `RemoteChannel`s which allows a process to be communicated with.
"""
struct WorkerComms
  worker_in::RemoteChannel
  worker_out::RemoteChannel
end
WorkerComms(wid=myid()) =
  WorkerComms(RemoteChannel(wid), RemoteChannel(wid))

global const COMMS = Ref{WorkerComms}()

set_comms!(comms) =
  COMMS[] = comms
get_comms() = COMMS[]

Base.put!(comms::WorkerComms, value) =
  put!(comms.worker_out, value)
Base.take!(comms::WorkerComms) =
  take!(comms.worker_in)
Base.isready(comms::WorkerComms) =
  isready(comms.worker_in)

"Flips the channels within a `WorkerComms`, for passing to another process."
Base.reverse(comms::WorkerComms) =
  WorkerComms(comms.worker_out, comms.worker_in)

Base.wait(comms::WorkerComms) =
  wait(comms.worker_in)

"""
    RPCPacket

A packet with a unique ID, an action symbol, and an arbitrary data payload.
"""
struct RPCPacket
  id::UUID
  action::Symbol
  data
end
RPCPacket(action, data) = RPCPacket(uuid4(), action, data)

"""
    RPCServer

A simple RPC server which permits both peer-to-peer and broadcast semantics.
"""
struct RPCServer
  comms::WorkerComms
  queue::Vector{Tuple{Symbol,Any}}
end
RPCServer(comms) = RPCServer(comms, Tuple{Symbol,Any}[])

recv!(rpc::RPCServer, pkt) =
  push!(rpc.queue, pkt)

function waitfor(rpc::RPCServer, act::Symbol)
  while true
    idx = findfirst(pkt->pkt.action==act, rpc.queue)
    if idx !== nothing
      # FIXME: Remove packet from queue
      return pkt
    end
    wait(rpc.comms)
  end
end

function cluster_update_state(state)
  for (w,comms) in CE_WORKERS[]
    put!(comms, (:cluster_update_state, state))
  end
end


function worker_announce_configuration(comms)
  # FIXME: Gather hardware/software/etc. configuration
  # FIXME: Send configuration to server/workers
end
function worker_init_config(comms)
  put!(comms, (:worker_init_config,))
  take!(comms)[2]
end
function worker_update_state(comms, state)
  # FIXME: Exchange data (frontier, etc.) with other workers
  put!(comms, (:network_update_state, state))
  take!(comms)
end
