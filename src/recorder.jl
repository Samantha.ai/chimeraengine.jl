export generate_initial_history, record!, walk_record

#= TODO: History Format
Per-worker
  ModelTransformer transitions
  Optimizer transitions

In each transition record
  Timestamp (down to millisecond)
  Relative path to file
=#
struct Record{HR,DR}
  history_recorder::HR
  data_recorder::DR
end
Record(::Type{HR}, history_config, ::Type{DR}, data_config) where {HR,DR} =
  Record(HR(history_config), DR(data_config))

# FIXME: Provide traits for indicating the type of initial config necessary
# FIXME: e.g. DB connection details, file paths, etc.

function generate_initial_history()
  return (
    timestamp = now(),
    datapath = "",
  )
end

function initialize_record(config)
  history_recorder = config["history_recorder"]
  history_config = get(config, "history_config", NamedTuple())
  data_recorder = config["data_recorder"]
  data_config = get(config, "data_config", NamedTuple())
  Record(history_recorder, history_config, data_recorder, data_config)
end

record!(record::Record, data) =
  record!(record.history_recorder, record.data_recorder, data)

function record_frontier!(record::Record, frontier, added, deleted)
  for set in deleted.sets
    if !any(s->s.uuid==set.uuid, frontier.sets)
      # FIXME: Forcibly remove set folder
    end
  end
  added_path = joinpath(frontier_path, string(added.uuid))
  if !isdir(added_path)
    mkdir(added_path)
    # FIXME: Update frontier table
    # FIXME: Record set table
  end
  for cand in added.candidates
    cand_path = joinpath(added_path, string(cand.uuid))
    # FIXME: Record data
  end
end

function walk_record(func::Function, record::Record, watch=false)
  result = Base.iterate(record)
  while true
    if result !== nothing
      (history_entry, data_entry), state = result
      func(history_entry, data_entry)
    else
      if !watch
        return
      else
        @error "FIXME: Wait for updates"
        continue
      end
    end
    result = Base.iterate(record, state)
  end
end

function Base.iterate(record::Record, state=nothing)
  if state === nothing
    result = Base.iterate(record.history_recorder)
  else
    result = Base.iterate(record.history_recorder, state)
  end
  result === nothing && return nothing
  history_entry, state = result
  data_entry = record.data_recorder[history_entry]
  return (history_entry, data_entry), state
end
Base.length(record::Record) =
  length(record.history_recorder)
Base.eltype(record::Record) =
  Tuple{eltype(record.history_recorder), eltype(record.data_recorder)}
