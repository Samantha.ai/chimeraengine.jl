export set_hook!, clear_hook!

global const HOOKS = Dict{Symbol,Any}()

function do_hook!(key::Symbol)
  if haskey(HOOKS, key)
    HOOKS[key]()
  end
end
# TODO: Functions for set_hook! and clear_hook! on remote worker
function set_hook!(func::Function, key::Symbol)
  HOOKS[key] = func
end
function clear_hook!(key::Symbol)
  if haskey(HOOKS, key)
    delete!(HOOKS, key)
  end
end
function clear_hook!()
  for key in HOOKS
    delete!(HOOKS, key)
  end
end
