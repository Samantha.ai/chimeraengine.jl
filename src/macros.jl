export @ce_import
export @ce_iface_def, @ce_iface_def_array, @ce_obj_def, @ce_param_def, @ce_runner, @ce_problem
export @ce_iface, @ce_obj, @ce_param, @ce_debug, @ce_runmodel

macro ce_import()
  :(import ChimeraEngine: generate_model, optimize_model, policy_init!, policy_update!, ce_run_model, apply_params)
end

macro ce_iface_def(name, dir, typ)
  :(InterfaceDefinition($(QuoteNode(name)), $dir, $typ, nothing))
end
macro ce_iface_def_array(name, dir, typ, sz)
  :(InterfaceDefinition(
    $(QuoteNode(name)),
    $dir,
    $typ,
    ArrayInterface($sz)
  ))
end
macro ce_obj_def(name, typ)
  :(ObjectiveDefinition($(QuoteNode(name)), $(esc(typ))))
end
macro ce_param_def(name, typ, default=nothing)
  :(ParameterDefinition($(QuoteNode(name)), $(esc(typ)), $(esc(default))))
end
macro ce_runner(name, ex)
  :(($name, CE_PARAMETERS=nothing) -> begin
    CE_OBJECTIVES = Dict{Symbol,Any}()
    $ex
    return CE_OBJECTIVES
  end)
end

function _process_prob_def(args)
  for arg in args
    println(typeof(arg))
  end
  args = [args...]
  args = filter(a->a !== nothing, args)
  iface_defs = filter(a->a isa InterfaceDefinition, args)
  # FIXME: Assert that interface names are unique
  obj_defs = filter(a->a isa ObjectiveDefinition, args)
  # FIXME: Assert that objective names are unique
  # FIXME: Assert that only one runner exists
  runner = first(filter(a->a isa Function, args))
  ProblemDefinition(iface_defs, obj_defs, runner)
end
#=
macro ce_problem(ex)
  :(_process_prob_def($ex))
end
=#
macro ce_problem(ex)
  quote
    args = $ex
    for arg in args
      println(typeof(arg))
    end
    args = [args...]
    args = filter(a->a !== nothing, args)
    iface_defs = filter(a->a isa InterfaceDefinition, args)
    # FIXME: Assert that interface names are unique
    obj_defs = filter(a->a isa ObjectiveDefinition, args)
    # FIXME: Assert that objective names are unique
    # FIXME: Assert that only one runner exists
    runner = first(filter(a->a isa Function, args))
    ProblemDefinition(iface_defs, obj_defs, runner)
  end
end

function ce_get_interface(model_def::ModelDefinition, name::Symbol)
  iface_def = model_def.iface_defs[findfirst(id->id.name==name, model_def.iface_defs)]
  InterfaceView(model_def.model, iface_def.name)
end

macro ce_iface(model, name)
  :(ce_get_interface($(esc(model)), $(QuoteNode(name))))
end
macro ce_obj(meta, name)
  :($(esc(meta)).objectives[$(QuoteNode(name))] = $(esc(name)))
end
macro ce_obj(meta, name, value)
  :($(esc(meta)).objectives[$(QuoteNode(name))] = $(esc(value)))
end
macro ce_param(meta, name)
  :($(esc(meta)).parameters[$(QuoteNode(name))])
end
macro ce_debug(meta, name, ex)
  quote
    debug_hook = $(esc(meta)).debug_hook
    name = $(QuoteNode(name))
    val_name = Val(name)
    if debug_hook !== nothing
      debug_hook(val_name, $(esc(ex)))
    end
  end
end
macro ce_runmodel(model)
  :(ce_run_model($(esc(model))))
end
