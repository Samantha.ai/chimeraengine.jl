export socket_listen

struct Controller
  config_path::String
  config::Dict{String,Any}
  sock::Sockets.PipeServer
  worker_comms::Dict{Int,WorkerComms}
  worker_conn_strings::Dict{String,Int}
end
function Controller(config_path::String)
  config = Dict{String,Any}()
  config["hosts"] = String[]
  config["project_path"] = nothing
  # TODO: We should do something for Windows I guess...
  mkpath("/tmp/chimeraengine")
  chmod("/tmp/chimeraengine", 0o700)
  sock = listen("/tmp/chimeraengine/controller.sock")
  return Controller(config_path, config, sock)
end
Controller(config_path::String, config, sock) = Controller(
  config_path,
  config,
  sock,
  Dict{Int,WorkerComms}(),
  Dict{String,Int}(),
)

function socket_listen(ctrl::Controller)
  while true
    @info "Controller waiting"
    conn = accept(ctrl.sock)
    schedule(@task begin
      try
        @info "Controller accepted"
        cmd = chomp(lowercase(readline(conn)))
        @info "Controller command: $cmd"
        @info Vector{UInt8}(cmd)
        if cmd == "reload"
          load_config!(ctrl)
        elseif cmd == "stop"
          stop_workers!(ctrl)
          exit(0)
        else
          @warn "Controller command invalid"
        end
        @info "Controller sleep"
      catch err
        @warn err
      end
    end)
  end
end

function load_config!(ctrl::Controller)
  config_path = ctrl.config_path
  old_config = ctrl.config
  config_file = open(config_path, "r+")
  new_config = TOML.parse(config_file)

  # Compare and adjust hosts
  delete_hosts = filter(host->!(host in new_config["hosts"]), old_config["hosts"])
  add_hosts = filter(host->!(host in old_config["hosts"]), new_config["hosts"])
  for host in delete_hosts
    rm_worker(ctrl, ctrl.worker_conn_strings[host])
    # FIXME: Cleanup entries in ctrl
  end
  for host in add_hosts
    if host == "localhost"
      wid = first(addprocs(1))
    else
      wid = first(addprocs(host))
    end
    add_worker(ctrl, wid)
    ctrl.worker_conn_strings[host] = wid
  end

  # Canonicalize project path
  project_path = new_config["project_path"]
  if !isabspath(project_path)
    project_path = normpath(joinpath(dirname(config_path), project_path))
  end
  @assert Base.Filesystem.samefile(project_path, new_config["project_path"])
  old_project_path = old_config["project_path"]
  # Compare and adjust project path
  if old_project_path === nothing || !Base.Filesystem.samefile(old_config["project_path"], project_path)
    # Send new project path to all workers
    for comms in values(ctrl.worker_comms)
      put!(comms, (:worker_project_path, new_config["project_path"]))
    end
  end

  merge!(ctrl.config, new_config)
  close(config_file)
end

function add_worker(ctrl::Controller, wid::Int)
  @everywhere [wid] begin
    @eval using ChimeraEngine
  end
  # FIXME: Cause this to execute in Main of the remote worker
  comms = remotecall_fetch(run_worker, wid)
  ctrl.worker_comms[wid] = comms
  return wid
end
function rm_worker(ctrl::Controller, wid::Int)
  comms = ctrl.worker_comms[wid]
  try
    @info "Stopping worker $wid"
    put!(comms, (:exit, nothing))
    t = rmprocs(wid; waitfor=1)
    wait(t)
    @info "Stopped worker $wid"
  catch err
    @warn "Failed to stop worker $wid"
    @warn err
    # FIXME: Kill worker manually
  end
end
function stop_workers!(ctrl::Controller)
  for (wid,comms) in ctrl.worker_comms
    try
      @info "Stopping worker $wid"
      put!(comms, (:exit, nothing))
      t = rmprocs(wid; waitfor=1)
      wait(t)
      @info "Stopped worker $wid"
    catch err
      @warn "Failed to stop worker $wid"
      @warn err
      # FIXME: Kill worker manually
    end
  end
end
