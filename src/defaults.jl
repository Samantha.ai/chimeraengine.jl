### Exports ###

export DumbModelTransformer, DumbOptimizer, DumbPolicy, DumbModel

### Types ###

struct DumbModelTransformer end
struct DumbOptimizer end
struct DumbPolicy end
struct DumbModel end

register!(:model_transformer, DumbModelTransformer)
register!(:optimizer, DumbOptimizer)
register!(:policy, DumbPolicy)
register!(:model, DumbModel)

### Methods ###

function generate_model(model_trans::DumbModelTransformer, iface_defs, model::DumbModel=DumbModel())
  return deepcopy(model), ([0.0], [(0.0, 0.0)]), nothing
end

function optimize_model(
  opt::DumbOptimizer,
  obj_defs,
  run_func,
  model,
  params,
  prob_params,
  debug_hook,
)
  model = apply_params(model, params)
  meta = (
    objectives = Dict{Symbol,Any}(),
    parameters = prob_params,
    debug_hook = debug_hook,
  )
  run_func(model, meta)
  objs = Float64[]
  for def in obj_defs
    push!(objs, meta.objectives[def.name])
  end
  return CandidateSet(model.model, [Candidate(params, objs)], obj_defs), nothing
end

function policy_init!(state, policy::DumbPolicy, config)
  state["model_trans_def"] = get(config,
    "initial_model_transformer",
    DumbModelTransformer())
  state["opt_def"] = get(config,
    "initial_optimizer",
    DumbOptimizer())
end
policy_update!(state, policy::DumbPolicy) = ()

get_params(model::DumbModel) =
  ([0.0], [(0.0, 0.0)])
apply_params(model::DumbModel, params::Vector{Float64}) =
  deepcopy(model)
Base.getindex(iv::ChimeraEngine.InterfaceView{DumbModel}) = 0.0
Base.setindex!(iv::ChimeraEngine.InterfaceView{DumbModel}, value::Float64) = ()
ce_run_model(model::DumbModel) = ()
