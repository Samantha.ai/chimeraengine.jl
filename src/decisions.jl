export decide

# Fallback for <: Number
decide(::Type{N}) where N<:Number =
  rand(N)
