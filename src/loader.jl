struct FileResource
  name::Symbol
  path::String
end

load_resource(res::String) =
  include(res)
load_resource(res::FileResource) =
  include(res.path)

function load_resources(config)
  state = Dict{Symbol, Any}()
  for (k,v) in config
    @info "Loading $k => $v"
    state[k] = load_resource(v)
  end
  state
end
