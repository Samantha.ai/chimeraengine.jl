@ce_problem begin
  # Interfaces
  @ce_iface_def(iface_in, :in, Float64),
  @ce_iface_def(iface_out, :out, Float64),

  # TODO: Analyses

  # Objectives
  @ce_obj_def(obj_1, Float64),
  @ce_obj_def(obj_2, Float64),

  @ce_runner model begin
    iface_in = @ce_iface model iface_in
    iface_out = @ce_iface model iface_out
    iface_in[] = 1.0
    @ce_runmodel model
    x = iface_out[]
    obj_1 = abs2(x)
    obj_2 = abs2(x - 2.0)
    @ce_obj obj_1
    @ce_obj obj_2
  end
end
