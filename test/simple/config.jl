using ChimeraEngine
@ce_import

struct SimpleModelTransformer end
struct SimpleOptimizer end
struct SimplePolicy end
struct SimpleModel
  data::Dict{Symbol,Float64}
end
SimpleModel() = SimpleModel(Dict{Symbol,Float64}())

register!(:model_transformer, SimpleModelTransformer)
register!(:optimizer, SimpleOptimizer)
register!(:policy, SimplePolicy)
register!(:model, SimpleModel)

# FIXME: This should instead be triggered from the outer loop itself
set_hook!(:inner_complete) do
  put!(ChimeraEngine.get_comms(), (:one_and_done,))
end

function generate_model(model_trans::SimpleModelTransformer, iface_defs, model::SimpleModel=SimpleModel())
  @assert length(filter(isinput, iface_defs)) == 1 "Wrong number of inputs"
  @assert length(filter(isoutput, iface_defs)) == 1 "Wrong number of outputs"
  @assert all(iface->iface.typ==Float64, iface_defs) "Wrong interface types"
  model.data[:in] = 0.0
  model.data[:out] = 0.0
  params = [1.0]
  return model, params, nothing
end

function optimize_model(opt::SimpleOptimizer, obj_defs, run_func, model, params)
  model = apply_params(model, params)
  results = run_func(model)
  return CandidateSet(model, [Candidate(params, results)]), nothing
end

function policy_init!(state, policy::SimplePolicy, config)
  state["model_trans_def"] = SimpleModelTransformer()
  state["opt_def"] = SimpleOptimizer()
end

function policy_update!(state, policy::SimplePolicy)
end

function apply_params(model::SimpleModel, params::Vector{Float64})
  model = deepcopy(model)
  model.data[:hidden] = first(params)
  model
end
function Base.getindex(iv::ChimeraEngine.InterfaceView{SimpleModel})
  key = if iv.sym == :iface_in
    :in
  elseif iv.sym == :iface_out
    :out
  end
  iv.model.data[key]
end
function Base.setindex!(iv::ChimeraEngine.InterfaceView{SimpleModel}, value::Float64)
  key = if iv.sym == :iface_in
    :in
  elseif iv.sym == :iface_out
    :out
  end
  iv.model.data[key] = value
end
function ce_run_model(model::SimpleModel)
  model.data[:out] = model.data[:in] * model.data[:hidden]
end

return Dict(
  "toplevel_policy" => SimplePolicy(),
  "history_recorder" => CSVRecorder,
  "history_config" => Dict(
    "dirpath" => joinpath(@__DIR__, "history"),
  ),
  "data_recorder" => BSONRecorder,
  "data_config" => Dict(
    "dirpath" => joinpath(@__DIR__, "data"),
  ),
)
