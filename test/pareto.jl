@testset "Pareto Optimality and Merging" begin
  frontier = FrontierCandidates[]
  incumbent1 = FrontierCandidates(Agent(), DataFrame(uuid=[uuid1()], obj1=[1.0], obj2=[2.0]))
  incumbent2 = FrontierCandidates(Agent(), DataFrame(uuid=[uuid1()], obj1=[1.1], obj2=[1.9]))
  SamanthaOptimizer.append_candidates!(frontier, incumbent1, incumbent2)

  challenger1 = FrontierCandidates(Agent(), DataFrame(uuid=[uuid1()], obj1=[0.9], obj2=[2.0]))
  challenger2 = FrontierCandidates(Agent(), DataFrame(uuid=[uuid1()], obj1=[0.9], obj2=[2.0]))

  @test dominates(challenger1.params[1,:], incumbent1.params[1,:], [:obj1, :obj2])
  @test !dominates(incumbent1.params[1,:], challenger1.params[1,:], [:obj1, :obj2])

  @test !dominates(challenger1.params[1,:], incumbent2.params[1,:], [:obj1, :obj2])
  @test !dominates(incumbent2.params[1,:], challenger1.params[1,:], [:obj1, :obj2])

  @test !dominates(challenger1.params[1,:], challenger2.params[1,:], [:obj1, :obj2])

  added, deleted = SamanthaOptimizer.pareto_merge!(frontier, challenger1, [:obj1, :obj2])
  @test !(incumbent1 in frontier)
  @test incumbent2 in frontier
  @test challenger1 in frontier
  @test nrow(added.params) == 1
  @test added.params[1,:] == challenger1.params[1,:]
  @test length(deleted) == 1 && nrow(deleted[1].params) == 1
  @test deleted[1].params[1,:] == incumbent1.params[1,:]

  added, deleted = SamanthaOptimizer.pareto_merge!(frontier, challenger2, [:obj1, :obj2])
  @test incumbent2 in frontier
  @test challenger1 in frontier
  @test challenger2 in frontier
  @test nrow(added.params) == 1
  @test added.params[1,:] == challenger2.params[1,:]
  @test length(deleted) == 0
end
