using ChimeraEngine
using Test

@testset "Simple Demo" begin
  ctrl = demo_main("simple/")

  # FIXME: Test that an iteration of the optimizer completed without error properly
  # FIXME: Extend timeout to 1 minute per test or so
  wid = first(keys(ctrl.worker_comms))
  timedwait(() -> isready(ctrl.worker_comms[wid]), 30.)
  if isready(ctrl.worker_comms[wid])
    @test take!(ctrl.worker_comms[wid])[1] == :one_and_done
  else
    @test "Failed to receive one-and-done signal from worker"
  end
end
